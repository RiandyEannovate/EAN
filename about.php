<?php 
include("packages/require.php");
$curpage='about';
$cur_url='about.html';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-about'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-about'];?>">
	<meta name="description" content="<?php echo $seo['desc-about'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/about.css?<?=mt_rand(10,1000);?>" media="screen"/>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
	<script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<div class="header-work top-height">
		<div class="container container-ean">
			<div class="header-wposition">
				<div class="header-wcontent">
					<div class="header-wnote">EANNOVATE - a Full Service Creative Digital Agency based in Jakarta</div>
					<div class="header-wtext">
						OUR TEAM ARE MORE THAN HAPPY TO ASSIST YOU
					</div>
					<div class="header-wborder"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END SECTION HEADER -->

	<div class="section-about">
		<div class="container container-about">

			<div class="part-about">
				<div class="about-head">
					WE FOCUS ON DELIVERING INNOVATION FOR YOUR BUSINESS
				</div>
				<div class="about-desc">
					Digitally, We are strong in Company Profile, E-Commerce,
					Custom Website and Mobile Apps Development
					<br/><br/>
					Visually, We are more than capable in generation outstanding
					Creative Content, Graphic and Video Production
					<br/><br/>
					We are well known for our ability in combining Creativity,
					Technology and Commercialization to Deliver Innovation.
				</div>
				<div class="about-contact">
					<a href="#modalContact" class="btn-acontact" data-toggle="modal" data-target="#modalContact">CONTACT US</a>
				</div>
			</div>

			<div class="part-about-client">
				<div class="aclient-head">
					<span>OUR CLIENTS</span>
					<hr/>
				</div>
				<div class="client-slider">
					<div id="swiper-client" class="swiper-container sclient-container">
						<div class="swiper-wrapper sclient-wrapper">

							<div class="swiper-slide sclient-slide">
								<?php for($c=1;$c<=12;$c++){ ?>
								<div class="sclient-image">
									<a href="#" style="background-image: url('<?=$global['absolute-url'].'img/client/client-'.$c.'.png';?>')"></a>
								</div>
								<?php if($c % 3 == 0){ ?>
								</div>
								<div class="swiper-slide sclient-slide">
								<?php } ?>
								<?php } ?>
							</div>

						</div>

					</div>
				</div>
			</div>

			<div class="part-team-client">
				<div class="aclient-head">
					<span>OUR TEAM</span>
					<hr/>
				</div>
				<div class="team-row">
					<?php for($c=1;$c<=4;$c++){ ?>
					<div class="team-col">
						<div class="team-warp">
							<div class="team-image" style="background-image: url('<?=$global['absolute-url'].'img/face.png';?>')"></div>
							<div class="team-name">Bill Gates</div>
							<div class="team-job">Founder Eannovate</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>

		</div>
	</div>
	
	
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->

	<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog cmodal-dialog" role="document">
	    	<div class="modal-content cmodal-dialog">
	      		<div class="modal-body cmodal-body">
	        		<div class="cmodal-head">CONTACT US</div>
	        		<div class="cmodal-note">Our team are more than happy to help you</div>
	        		<div class="cmodal-form">
		        		<form id="form-contact" name="contact" action="about.php?action=contact" enctype="multipart/form-data" method="post" onsubmit="return validContact()">
		        			<div id="cinput-error" class="error-text"></div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-name" name="name" class="form-control cmodal-input" placeholder="your name">
		        			</div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-email" name="email" class="form-control cmodal-input" placeholder="your e-mail address">
		        			</div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-subject" name="subject" class="form-control cmodal-input" placeholder="what it is about">
		        			</div>
		        			<div class="cmodal-content">
		        				<textarea rows="6" id="cinput-message" name="message" class="form-control cmodal-input" placeholder="your message"></textarea>
		        			</div>
		        			<div class="cmodal-submit">
		        				<button type="submit" class="btn btn-submit">SEND</button>
		        			</div>
		        		</form>
	        		</div>
	      		</div>
	    	</div>
	  	</div>
	</div>
	
	<script src="<?=$global['absolute-url'];?>js/about.js?<?=mt_rand(10,1000);?>"></script>
</body>
</html>