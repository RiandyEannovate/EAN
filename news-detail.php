<?php 
include("packages/require.php");
$curpage='news';
$cur_url='news/';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-news'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-news'];?>">
	<meta name="description" content="<?php echo $seo['desc-news'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/news-detail.css?<?=mt_rand(10,1000);?>" media="screen"/>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<div class="header-dnews top-height">
		<div class="container container-ean">
			<div class="header-dnposition">
				<div class="header-dncontent">
					<div class="header-dncategory">INFORMATION TECHNOLOGY</div>
					<div class="header-dntitle">
						MOBILE INI DAPAT BERJALAN DI DARAT DAN DI UDARA!
					</div>
					<div class="header-dnwriter">
						<span>By Billy Gani</span> 14 April 2017
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END SECTION HEADER -->

	<div class="section-news">
		<div class="container container-ean">
			<div class="dnews-back">
				<a href="<?=$path['news'];?>">
					<img src="<?=$global['absolute-url'].'img/arrow-left.png';?>" alt="icon"> E-NEWS
				</a>
			</div>
		</div>

		<div class="dnews-image">
			<img src="<?=$global['absolute-url'].'img/image-news.png';?>" alt="image">
		</div>

		<div class="container container-dnews">
			<div class="dnews-desc">
				Mobil merupakan sarana transportasi terbanyak yang digunakan seluruh masyarakat dunia. Dampaknya, kemacetan terjadi di mana - mana. Lalu, bagaimana jika untuk mengatasi kemacetan mobil di darat, anda cukup menggunakan mobil terbang?
				<br/><br/>
				Perusahaan di Slowakia, yaitu AeroMobi menciptakan mobil terbang. Ini bukanlah hal baru, sebab pada tahun 2014 perusahaan ini sudah membuatnya namun kurang mendapat pemberitaan dan perhatian dari dunia.
				<br/><br/>
				Namun kini, AeroMobi mengumumkan design mobil terbang terbarunya dan mulai diperkenalkan pada ajang pamrtan supercar dunia, Top Marques Monaco pda 20 April 2017.
				<br/><br/>
				Kendaraan masa depan ini dapat berguna sebagai mobil roda empat dan juga pesawat terbang sekaligus dengan menggunakan mesin hybrid sehingga tidak merusak lingkungan dan lebih efisien yang memungkinkan perjalanan dari pintu ke pintu secara lebih cepat untuk jarak menengah dan terbatas.
				<br/><br/>
				Nantinya, pemilik mobil terbang ini akan membutihkan SIM dan juga lisensi pilot untuk dapat mengendarai mobil terbang ini. 
				<br/><br/>
				Siapkan dana anda secara besar - besaran, karena untuk memilikinya anda harus merogoh kocek sebesar US$ 3,2 juta atau setara dengan Rp 42,5 miliar. Waw, sungguh harga yang fantastis!
			</div>
			<div class="eannovate-ads">
				<div class="eads-border">
					<hr/>
				</div>
				<div class="eads-row">
					<div class="eads-col-img">
						<a href="#">
							<img src="<?=$global['absolute-url'].'img/logo/logo-eannovate-black.png';?>" alt="logo eannovate">
						</a>
					</div>
					<div class="eads-col">
						<div class="eads-desc">
							Provides Creative Solution for your problem. Our Services range from Branding, Graphic Design, Video Animation, Web design, development and Mobile Apps.
						</div>
					</div>
				</div>
				<div class="eads-border">
					<hr/>
				</div>
			</div>
			<div class="dnews-subscribe">
				<div class="dnsub-title">
					SUBSCRIBE FOR MORE
				</div>
				<div class="dnsub-form">
					<form id="form-contact" name="contact" action="news-detail.php?action=subscribe" enctype="multipart/form-data" method="post" onsubmit="return validSubs()">
						<input type="text" class="form-control dnsub-input" id="subs-email" name="email" placeholder="YOUR E-MAIL ADDRESS" autocomplete="off">
						<input type="hidden" name="news_title" value="title">
						<input type="hidden" name="news_id" value="1">
						<div id="subs-error" class="error-text text-center"></div>
					</form>
				</div>
			</div>
		</div>

		<div class="section-related-work">
			<div class="container container-rnwork">
				<div class="rnwork-head">RELATED WORK</div>
				<div class="rnwork-list">
					<div class="row-rnwork">
						<?php for($w=1;$w<=2;$w++){ ?>
						<div class="col-rnwork">
							<div class="rnwork-wrap">
								<a href="<?=$path['work-detail'];?>" class="rnwork-image" style="background-image: url('<?=$global['absolute-url'].'img/news5.png';?>')"></a>
								<div class="rnwork-content">
									<div class="rnwork-category">CLIENT WORK</div>
									<a href="<?=$path['work-detail'];?>" class="rnwork-title">KANEKIN.CO</a>
									<div class="rnwork-desc">
										Kanekin initiated as a creative agency handling
										brand experience design and marketing of various
										brands in indonesia
									</div>
									<div class="rnwork-link">
										<a href="<?=$path['work-detail'];?>">
											<span>SEE MORE</span>
											<hr/>
										</a>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="section-related-news">
			<div class="container container-rnews">
				<div class="rnews-head">SEE ALSO</div>
				<div class="rnews-list">
					<div class="row-rnews">
						<?php for($w=1;$w<=2;$w++){ ?>
						<div class="col-rnews">
							<div class="rnews-wrap">
								<div class="rnews-bg" style="background-image: url('<?=$global['absolute-url'].'img/news3.png';?>');">
									<div class="rnews-content">
										<div class="rnews-title">
											Eannovate - Solusi Digital Marketing
										</div>
										<div class="rnews-desc">
											Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
											<a href="<?=$path['news-detail'];?>" class="rnews-read">read more</a>
										</div>
										<div class="rnews-category">
											<span>Gadget</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

	</div>
	
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->

	<script src="<?=$global['absolute-url'];?>js/news-detail.js?<?=mt_rand(10,1000);?>"></script>
</body>
</html>