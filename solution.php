<?php 
include("packages/require.php");
$curpage='solution';
$cur_url='solution.html';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-solution'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-solution'];?>">
	<meta name="description" content="<?php echo $seo['desc-solution'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/solution.css?<?=mt_rand(10,1000);?>" media="screen"/>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<div class="header-work top-height">
		<div class="container container-ean">
			<div class="header-wposition">
				<div class="header-wcontent">
					<div class="header-wnote">OUR SOLUTION FOR YOUR CREATIVE PROBLEM</div>
					<div class="header-wtext">
						WHAT WE CAN DO
					</div>
					<div class="header-wborder"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END SECTION HEADER -->

	<div class="section-solution">
		<div class="container container-solution">
			<div class="solution-head">
				OUR SOLUTION
			</div>

			<div class="solution-list">
				<div class="row-solution">

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										WEBSITE DEVELOPMENT
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-web.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								High-end Unique Web Designs<br/>
								Responsive and Viewable across browser<br/>
								PHP and Javascript Applications<br/>
								Custom CMS Solutions<br/>
								SEO Friendly 
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										DIGITAL MARKETING
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-digital.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Authentic Article & Content<br/>
								Social Media Activation<br/>
								Social Media Campaign
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										MOBILE APPS DEVELOPMENT
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-mobile.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								IOS & Android Platform<br/>
								API Development<br/>
								UI/UX Consultation
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										PRODUCT VIZUALISATION
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-product.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Concepting<br/>
								Motion Graphics<br/>
								3D Animation<br/>
								Video Editing<br/>
								Visual Effects
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										E-COMMERCE WEB DEVELOPMENT
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-ecommerce.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Jasa pembuatan website E-Commerce atau online shop bagi anda yang berencana
								memulai / memperluas jaringan usaha
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										BRAND IDENTITY
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-brand.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Logo<br/>
								Business Card<br/>
								Stationary / Marketing Material<br/>
								Event Marketing<br/>
								Desain Website
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										AUGMENTED REALITY
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-virtual.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Architectural Building & Structure<br/>
								Automotive Design<br/>
								Character Design<br/>
								Product Design<br/>
								Product Illustration
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										GRAPHIC DESIGN
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-video.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Packaging Design<br/>
								Ads / Promotional Poster<br/>
								Banner Design<br/>
								Annual Book<br/>
								Infographics
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										UI / UX SOLUTION
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-game.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Website Re-design<br/>
								Best Pratice Interface Design<br/>
								8 Golden Rules
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

					<div class="col-solution">
						<div class="solution-wrap">
							<div class="row-tsol">
								<div class="col-tsol-left">
									<a href="#" class="solution-title">
										NEWS MOBILE APPS
									</a>
								</div>
								<div class="col-tsol-right">
									<div class="solution-icon">
										<img src="<?=$global['absolute-url'].'img/sol-news.png';?>" alt="icon">
									</div>
								</div>
							</div>
							<div class="solution-desc">
								Your News App in Android & IOS<br/>
								News & Article from your current website<br/>
								Push Notification for your users<br/>
								AFFORDABLE starts from US$ 75 / month / platform<br/>
								Fast setup! App ready in less than 1 week
							</div>
							<div class="solution-arrow">
								<a href="#" class="solution-link">
									<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
								</a>
							</div>
						</div>
					</div>

				</div>	
			</div>

		</div>
	</div>
	
	
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->
</body>
</html>