function validSubs(){
	var email = $("#subs-email").val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(email != ""){
	    if(email.match(mailformat)){ 
			$("#subs-email").removeClass("error-cinput");
	    } else {
	      	$("#subs-email").focus();
			$("#subs-email").addClass("error-cinput");
			$("#subs-error").html("Incorrect email format!");
			$("#subs-error").show();
	      	return false;
	    }
	} else {
	   	$("#subs-email").focus();
		$("#subs-email").addClass("error-cinput");
		$("#subs-error").html("Please insert your email");
		$("#subs-error").show();
	    return false;
	}
}