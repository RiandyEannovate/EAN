function validContact(){
	var name = $("#cinput-name").val();
	var email = $("#cinput-email").val();
	var subject = $("#cinput-subject").val();
	var message = $("#cinput-message").val();
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(name != ""){
		$("#cinput-name").removeClass("error-cinput");
		$("#cinput-error").html("");
		$("#cinput-error").hide();
	} else {
		$("#cinput-name").focus();
		$("#cinput-name").addClass("error-cinput");
		$("#cinput-error").html("Please insert your name");
		$("#cinput-error").show();
		return false;
	}
	if(email != ""){
	    if(email.match(mailformat)){ 
			$("#cinput-email").removeClass("error-cinput");
	    } else {
	      	$("#cinput-email").focus();
			$("#cinput-email").addClass("error-cinput");
			$("#cinput-error").html("Incorrect email format!");
			$("#cinput-error").show();
	      	return false;
	    }
	} else {
	   	$("#cinput-email").focus();
		$("#cinput-email").addClass("error-cinput");
		$("#cinput-error").html("Please insert your email");
		$("#cinput-error").show();
	    return false;
	}
	if(subject != ""){
		$("#cinput-subject").removeClass("error-cinput");
	} else {
		$("#cinput-subject").focus();
		$("#cinput-subject").addClass("error-cinput");
		$("#cinput-error").html("Please insert your subject");
		$("#cinput-error").show();
		return false;
	}
	if(message != ""){
		$("#cinput-message").removeClass("error-cinput");
	} else {
		$("#cinput-message").focus();
		$("#cinput-message").addClass("error-cinput");
		$("#cinput-error").html("Please insert your message");
		$("#cinput-error").show();
		return false;
	}
}
$(document).ready(function(){
	var swiperClient = new Swiper('#swiper-client', {
	    autoplay :  10000,
	    speed: 800,
	    slidesPerView: 4,
	    spaceBetween: 15,
	    loop: false,
	    autoplayDisableOnInteraction:false
	});
	function FixSize(){
		if ($(window).width() > 991) {
            swiperClient.params.slidesPerView = 4;
        }
        if ($(window).width() < 991) {
            swiperClient.params.slidesPerView = 3;
        }
        if ($(window).width() < 767) {
            swiperClient.params.slidesPerView = 3;
        }
        if ($(window).width() < 500) {
            swiperClient.params.slidesPerView = 2;
        }
        swiperClient.update();
    }
    FixSize();
    $(window).resize(function(){
        FixSize();
    });
});