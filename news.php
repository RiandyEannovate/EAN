<?php
include("packages/require.php");
$curpage='home';
$cur_url='';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-home'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
	<meta name="description" content="<?php echo $seo['desc-home'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
			<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/news.css"/>
	<script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<?php include("parts/part-header.php");?>
	<!-- END SECTION HEADER -->


		<!-- START PRODUCT SECTION -->
		<div id="section-product">
			<div class="product-wrap">
				<div class="container container-ids">
					<div class="row">

						<div class="col-md-9 col-sm-8 col-xs-12 ">
							<div class="warp-ennews">

									<div class="row img-news-bg">
										<div class="col-xs-7 background-enews" style="background-image:url('img/e-news1.jpg');">

										</div>
										<div class="col-xs-5 background-enews" style="background-color: #090909;">
											<div class="margin-enews">
		 										<div class="category-enews">
		 											INFORMATION TECHNOLOGY
		 										</div>
		 										<div class="tittle-enews">
		 											MOBIL INI DAPAT BERJALAN DI DARAT DAN DI UDARA!
		 										</div>

		 										<div class="name-date-enews">
		 											<b>By Billy Gani</b> - 14 April 2017
		 										</div>

		 										<div class="text-enews">
		 											Kendaraan masa depan ini dapat berguna sebagai mobil roda empat dan juga pesawat terbang sekaligus dengan menggunakan...
		 										</div>
		 										<div class="text-right fa-bot-right">
		 											<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
		 										</div>
		 									</div>
										</div>
									</div>

									<div class="row img-news-bg">
										<div class="col-xs-7 background-enews" style="background-image:url('img/e-news2.jpg');">

										</div>
										<div class="col-xs-5 background-enews" style="background-color: #090909;">
											<div class="margin-enews">
		 										<div class="category-enews">
		 											INFORMATION TECHNOLOGY
		 										</div>
		 										<div class="tittle-enews">
		 											MOBIL INI DAPAT BERJALAN DI DARAT DAN DI UDARA!
		 										</div>

		 										<div class="name-date-enews">
		 											<b>By Billy Gani</b> - 14 April 2017
		 										</div>

		 										<div class="text-enews">
		 											Kendaraan masa depan ini dapat berguna sebagai mobil roda empat dan juga pesawat terbang sekaligus dengan menggunakan...
		 										</div>
		 										<div class="text-right fa-bot-right">
		 											<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
		 										</div>
		 									</div>
										</div>
									</div>
									<div class="row img-news-bg">
										<div class="col-xs-7 background-enews" style="background-image:url('img/e-news3.jpg');">

										</div>
										<div class="col-xs-5 background-enews" style="background-color: #090909;">
											<div class="margin-enews">
		 										<div class="category-enews">
		 											INFORMATION TECHNOLOGY
		 										</div>
		 										<div class="tittle-enews">
		 											MOBIL INI DAPAT BERJALAN DI DARAT DAN DI UDARA!
		 										</div>

		 										<div class="name-date-enews">
		 											<b>By Billy Gani</b> - 14 April 2017
		 										</div>

		 										<div class="text-enews">
		 											Kendaraan masa depan ini dapat berguna sebagai mobil roda empat dan juga pesawat terbang sekaligus dengan menggunakan...
		 										</div>
		 										<div class="text-right fa-bot-right">
		 											<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
		 										</div>
		 									</div>
										</div>
									</div>
									<div class="row img-news-bg">
										<div class="col-xs-7 background-enews" style="background-image:url('img/e-news4.jpg');">

										</div>
										<div class="col-xs-5 background-enews" style="background-color: #090909;">
											<div class="margin-enews">
		 										<div class="category-enews">
		 											INFORMATION TECHNOLOGY
		 										</div>
		 										<div class="tittle-enews">
		 											MOBIL INI DAPAT BERJALAN DI DARAT DAN DI UDARA!
		 										</div>

		 										<div class="name-date-enews">
		 											<b>By Billy Gani</b> - 14 April 2017
		 										</div>

		 										<div class="text-enews">
		 											Kendaraan masa depan ini dapat berguna sebagai mobil roda empat dan juga pesawat terbang sekaligus dengan menggunakan...
		 										</div>
		 										<div class="text-right fa-bot-right">
		 											<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
		 										</div>
		 									</div>
										</div>
									</div>

							</div>



							<div class="col-xs-12 button-see-more">

									<button type="button" class="btn btn-default" name="button" style="    padding: 10px 50px;">SEE MORE</button>

							</div>
						</div>


						<div class="col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 28px;">

							<div class="brand-list" data-aos="fade-up" data-aos-duration="1000">

								<div class="warp-hot-enews">
									<div class="enews-list-head">
										HOT NEWS
									</div>
									<div>
										<div class="img-hot-news" style="background-image:url('img/e-news4.jpg');"> </div>
										<div class="img-hot-news" style="background-image:url('img/e-news3.jpg');"> </div>
										<div class="img-hot-news" style="background-image:url('img/e-news2.jpg');"> </div>
										<div class="img-hot-news" style="background-image:url('img/e-news1.jpg');"> </div>
									</div>
								</div>


								<div class="enews-list-head">
									CATEGORIES
								</div>
								<div class="brand-wrap">
									<ul id="category-enews-list">
										<li class="active"><a href="#">Creative technology</a></li>
										<li><a href="#">Design and Art Technology</a></li>
										<li><a href="#">Information Technology (IT)</a></li>
										<li><a href="#">Entrepeneur</a></li>
										<li><a href="#">Gadget</a></li>
										<li><a href="#">Mobile OS</a></li>
										<li><a href="#">Digital Marketing</a></li>
										<li><a href="#">Application</a></li>
										<li><a href="#">Event</a></li>
										<li><a href="#">Client</a></li>

									</ul>
								</div>


							</div>







						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="popup-cart">
			<div class="popup-wrap">
				<div class="popup-text"></div>
			</div>
		</div>

		<script type="text/javascript">
			function show_popup(status,param){
				if(status != "failed"){
					$(".popup-wrap").removeClass("popup-failed");
				} else {
					$(".popup-wrap").addClass("popup-failed");
				}
				$(".popup-text").text(param);
				$(".popup-cart").fadeIn();
				setTimeout(function(){
					$(".popup-cart").fadeOut();
				}, 3000);
			}

			$(document).ready(function(){
				var swiperProduct = new Swiper('#swiper-product', {
				    autoplay :  false,
				    speed: 800,
				    slidesPerView: 8,
				    spaceBetween: 15,
				    loop: false,
	    			prevButton: '.swiper-pprev',
	    			nextButton: '.swiper-pnext',
				    autoplayDisableOnInteraction:false
				});

				function FixSize(){
					if ($(window).width() > 991) {
			            swiperProduct.params.slidesPerView = 8;
			        }
			        if ($(window).width() < 991) {
			            swiperProduct.params.slidesPerView = 5;
			        }
			        if ($(window).width() < 700) {
			            swiperProduct.params.slidesPerView = 4;
			        }
			        if ($(window).width() < 600) {
			            swiperProduct.params.slidesPerView = 3;
			        }
			        if ($(window).width() < 500) {
			            swiperProduct.params.slidesPerView = 2;
			        }
			        swiperProduct.update();
			    }

			    FixSize();
			    $(window).resize(function(){
			        FixSize();
			    });
			});

			function checkedList(){
			    var css = $('.brand-number');
			    var el = css.length;
			    var id = "";

			    for(var num = 1; num <= el; num++){
			    	var brand_id = $("#input-brand-"+num).val();
			        var cheklist = document.getElementById('input-brand-'+num);
			        if(cheklist.checked){
			            id += "," + brand_id;
			        }
			    }

			    var url = '<?=$global['absolute-url'].$lang_param.'product/1/';?>';
			    var carearea_title = '<?=$O_carearea;?>';
			    var carearea_id = '<?=$O_carearea_id;?>';
			    var brand = id != "" ? id.substring(1) : "all";
			    var link = url + brand + "/" + carearea_title + "_" + carearea_id + "/";
			    window.location = link;
			}
			$(".notbookmark").click(function(){
				$(".popup-wrap").addClass("popup-failed");
				$(".popup-text").html("You Must Login First To Boomark This Product!<div class='login-pop'><a href='<?=$path['login'];?>'>Login</a></div>");
				$("#notif-pop").fadeIn();
				setTimeout(function(){
					$("#notif-pop").fadeOut();
				}, 4000);
			});
			<?php if(isset($_SESSION['member_id'])){ ?>
			$(".bookmark").click(function(){
				var item_id = $(this).data("item");
				var item_type = "product";
				var member = "<?=$_SESSION['member_id'];?>";
				var member_code = "<?=$_SESSION['member_code'];?>";
				var url = "<?=$api['update-bookmark'];?>";
				var data = {
			      	user_id : member,
			      	auth_code : member_code,
			      	type_id : item_id,
			      	type : item_type
			    }
			    $.ajax({url: url,data : data, success:function(result){
			    	if(result.status == "200"){
			    		if($("#pbook-"+item_id).hasClass("active-pbook")){
			    			$("#pbook-"+item_id).removeClass("active-pbook");
			    		} else {
			    			$("#pbook-"+item_id).addClass("active-pbook");
			    		}

			    	} else {
			    		var status_popup = "failed";
			    		var text_popup = "Failed bookmark item";
			    		show_popup(status_popup,text_popup);
			    	}
			    }});
			});
			<?php } ?>
		</script>


	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->
</body>
</html>
