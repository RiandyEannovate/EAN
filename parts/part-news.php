<div class="section-news">
	<div class="container container-ean">
		<div class="sn-warp">
			<div class="sn-head">
				<div class="title">
					LATEST NEWS
				</div>
			</div>
			<div class="row-sn">-
				<div class="col-sn col-sn-first" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="sn-bg" style="background-image: url('<?=$global['absolute-url'].'img/news5.png';?>');">
						<div class="sn-content">
							<div class="sn-title">
								Eannovate - Solusi Digital Marketing
							</div>
							<div class="sn-desc">
								Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
								<a href="#" class="sn-read">read more</a>
							</div>
							<div class="sn-category">
								<span>Entrepreneurs</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sn" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="sn-bg" style="background-image: url('<?=$global['absolute-url'].'img/news1.png';?>');">
						<div class="sn-content">
							<div class="sn-title">
								Eannovate - Solusi Digital Marketing
							</div>
							<div class="sn-desc">
								Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
								<a href="#" class="sn-read">read more</a>
							</div>
							<div class="sn-category">
								<span>Gadget</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sn" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="sn-bg" style="background-image: url('<?=$global['absolute-url'].'img/news2.png';?>');">
						<div class="sn-content">
							<div class="sn-title">
								Eannovate - Solusi Digital Marketing
							</div>
							<div class="sn-desc">
								Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
								<a href="#" class="sn-read">read more</a>
							</div>
							<div class="sn-category">
								<span>Gadget</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sn" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="sn-bg" style="background-image: url('<?=$global['absolute-url'].'img/news3.png';?>');">
						<div class="sn-content">
							<div class="sn-title">
								Eannovate - Solusi Digital Marketing
							</div>
							<div class="sn-desc">
								Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
								<a href="#" class="sn-read">read more</a>
							</div>
							<div class="sn-category">
								<span>Gadget</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sn" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="sn-bg" style="background-image: url('<?=$global['absolute-url'].'img/news5.png';?>');">
						<div class="sn-content">
							<div class="sn-title">
								Eannovate - Solusi Digital Marketing
							</div>
							<div class="sn-desc">
								Perkembangan bisnis dalam usaha lini kecil di indonesia kini... 
								<a href="#" class="sn-read">read more</a>
							</div>
							<div class="sn-category">
								<span>Gadget</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="sn-more">
				<a href="<?=$path['news'];?>">MORE STORIES</a>
			</div>
		</div>
	</div>	
</div>