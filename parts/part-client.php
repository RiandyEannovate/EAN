<div class="section-client">
	<div class="row-client">
		<div class="col-client-right visible-xs visible-sm">
			<div class="sclient-wrap" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
				<div class="sclient-head">
					Our<br/>Clients
				</div>
				<div class="sclient-border">
					<hr/>
				</div>
			</div>
		</div>
		<div class="col-client-left">
			<div id="slide-client" class="swiper-container ssclient-container" data-aos="fade-up-right" data-aos-delay="100" data-aos-duration="1000">
				<div class="swiper-wrapper ssclient-wrapper">
					<div class="swiper-slide ssclient-slide">
						<div class="ssclient-image" style="background-image: url('<?=$global['absolute-url'].'img/img-client.png';?>');">
						</div>
					</div>
					<div class="swiper-slide ssclient-slide">
						<div class="ssclient-image" style="background-image: url('<?=$global['absolute-url'].'img/img-project.png';?>');">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-client-right hidden-xs hidden-sm">
			<div class="sclient-wrap" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
				<div class="sclient-head">
					Our<br/>Clients
				</div>
				<div class="sclient-border">
					<hr/>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var slideClient = new Swiper('#slide-client', {
	    autoplay :  8000,
	    speed: 800,
	    slidesPerView: 1,
	    effect: 'fade',
	    spaceBetween: 0,
	    loop: true,
	    autoplayDisableOnInteraction:false
	});
</script>