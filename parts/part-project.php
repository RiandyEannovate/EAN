<div class="section-project">
	<div class="row-project">
		<div class="col-project-left">
			<div class="sproject-wrap" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
				<div class="sproject-head">
					Our<br/>Projects
				</div>
				<div class="sproject-border">
					<hr/>
				</div>
				<div id="sproject-content" class="swiper-container spcontent-container">
					<div class="swiper-wrapper spcontent-wrapper">
						<div class="swiper-slide spcontent-slide">
							<div>
								<div class="sproject-desc-wrap">
									<div class="sproject-desc">
										TAMARA-TanyaAsmara adalah sebuah aplikasi mobile untuk saling berbagi tips dan pengalaman asmara yang berisi sebuah
										forum untuk menanyakan mengenai masalah asmara dan selain itu pula user juga dapat....
									</div>
								</div>
								<div class="sproject-link">
									<a href="#">
										<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
									</a>
								</div>
							</div>
						</div>
						<div class="swiper-slide spcontent-slide">
							<div>
								<div class="sproject-desc-wrap">
									<div class="sproject-desc">
										Monas adalah sebuah aplikasi mobile untuk saling berbagi tips dan pengalaman asmara yang berisi sebuah
										forum untuk menanyakan mengenai masalah asmara dan selain itu pula user juga dapat....
									</div>
								</div>
								<div class="sproject-link">
									<a href="#">
										<img src="<?=$global['absolute-url'].'img/icon-arrow-double.png';?>" alt="icon">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-project-right">
			<div id="slide-project" class="swiper-container ssproject-container" data-aos="fade-up-left" data-aos-delay="100" data-aos-duration="1000">
				<div class="swiper-wrapper ssproject-wrapper">
					<div class="swiper-slide ssproject-slide">
						<div class="ssproject-image" style="background-image: url('<?=$global['absolute-url'].'img/img-project.png';?>');">
						</div>
					</div>
					<div class="swiper-slide ssproject-slide">
						<div class="ssproject-image" style="background-image: url('<?=$global['absolute-url'].'img/img-client.png';?>');">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var slideProject = new Swiper('#slide-project', {
	    autoplay :  8000,
	    speed: 800,
	    slidesPerView: 1,
	    effect: 'fade',
	    spaceBetween: 0,
	    autoplayDisableOnInteraction:false
	});
    var projectContent = new Swiper('#sproject-content', {
	    speed: 800,
	    spaceBetween: 15,
	    centeredSlides: true,
        slideToClickedSlide: true
    });
    slideProject.params.control = projectContent;
    projectContent.params.control = slideProject;
</script>