<div class="section-footer">
	<div class="footer-wrap">
		<div class="container container-ean">
			<div class="footer-head">
				<div class="top">GET IN TOUCH</div>
				<a href="mailto:HELLO@EANNOVATE.COM" class="footer-mail" target="_blank"><?=strrev('HELLO@EANNOVATE.COM');?></a>
			</div>
			<div class="footer-border">
				<hr/>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="footer-flag">
						<img src="<?=$global['absolute-url'].'img/flag-indonesia.png';?>" alt="image">
					</div>
					<div class="footer-address">
						Ruko Kosambi Baru<br/>
						Blok A ext 1 no 66. Jakarta.
					</div>
					<div class="footer-phone">
						+62 21 5437 5601
					</div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="footer-flag">
						<img src="<?=$global['absolute-url'].'img/flag-germany.png';?>" alt="image">
					</div>
					<div class="footer-address">
						Berger Halde 11, 88079.<br/>
						Kressbronn am Bodensee.
					</div>
					<div class="footer-phone">
						+49 176 4561 8263
					</div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="footer-working-wrap">
						<div class="footer-working-img">
							<img src="<?=$global['absolute-url'].'img/footer-working.png';?>" alt="icon">
						</div>
						<div class="footer-working-head">
							WORKING HOURS
						</div>
						<div class="footer-working-desc">
							MON-FRI 9AM-5PM<br/>
							SAT 9AM-3PM
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container container-ean">
			<div class="row">
				<div class="col-md-7 col-sm-8 col-xs-12">
					<div class="copyright-text">
						Copyright &copy; 2017 PT Eannovate Creative Technology<br/>
						Website, Mobile Android And Ios App Design Development in Jakarta, Indonesia
					</div>
				</div>
				<div class="col-md-5 col-sm-4 col-xs-12">
					<div class="footer-social">
						<a href="#">
							<img src="<?=$global['absolute-url'].'img/footer-facebook.png';?>" alt="icon">
						</a>
						<a href="#">
							<img src="<?=$global['absolute-url'].'img/footer-twitter.png';?>" alt="icon">
						</a>
						<a href="#">
							<img src="<?=$global['absolute-url'].'img/footer-google.png';?>" alt="icon">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    AOS.init();
    $('.scroll').click(function() {
	    $('body').animate({
	        scrollTop: eval($('#' + $(this).attr('target')).offset().top - 70)
	    }, 1000);
	});
</script>