<div id="section-eannovate" class="section-eannovate">
	<div class="container container-ean">
		<div class="row-se">
			<div class="col-se-left">
				<div class="se-wrap" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
					<div class="se-bg"></div>
					<div class="se-content">
						<div class="title">WE ARE EANNOVATE</div>
						<div class="desc">
							We believe that excellent quality of product can only be achieved through the attention of the smallest details.<br/><br/>
							From one project to the next, we focus on product objectives and ensure each product will be designed and built based on its function and usage
						</div>
						<div class="se-link">
							<div class="col-selink">
								<div class="se-learnmore">
									<a href="<?=$path['about'];?>">LEARN MORE</a>
								</div>
							</div>
							<div class="col-selink">
								<div class="se-sosmed">
									<a href="#">
										<img src="<?=$global['absolute-url'].'img/icon-facebook.png';?>" alt="icon">
									</a>
									<a href="#">
										<img src="<?=$global['absolute-url'].'img/icon-twitter.png';?>" alt="icon">
									</a>
									<a href="#">
										<img src="<?=$global['absolute-url'].'img/icon-google.png';?>" alt="icon">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-se-right">
				<div class="se-solution-wrap hidden-xs">
					<table class="table-se">
						<tr>
							<td class="sol-left-side">
								<a href="#" class="se-sol-wrap sol-push-left" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-digital.png';?>" alt="icon">
									</div>
									<div class="se-sol-title">DIGITAL MARKETING</div>
								</a>
							</td>
							<td class="sol-center-side" rowspan="3" style="background-image: url('img/line-logo.png')">
								<div class="se-sol-center">
									<img src="<?=$global['absolute-url'].'img/sol-idea.png';?>" alt="icon" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
								</div>
							</td>
							<td class="sol-right-side">
								<a href="#" class="se-sol-wrap sol-push-right" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-title">WEBSITE DEVELOPMENT</div>
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-web.png';?>" alt="icon">
									</div>
								</a>
							</td>
						</tr>
						<tr>
							<td class="sol-left-side">
								<a href="#" class="se-sol-wrap" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-mobile.png';?>" alt="icon">
									</div>
									<div class="se-sol-title">MOBILE APPS DEVELOPMENT</div>
								</a>
							</td>
							<td class="sol-right-side">
								<a href="#" class="se-sol-wrap" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-title">VIRTUAL / AUGMENTED REALITY</div>
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-virtual.png';?>" alt="icon">
									</div>
								</a>
							</td>
						</tr>
						<tr>
							<td class="sol-left-side">
								<a href="#" class="se-sol-wrap sol-push-left" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-video.png';?>" alt="icon">
									</div>
									<div class="se-sol-title">VIDEO ANIMATION</div>
								</a>
							</td>
							<td class="sol-right-side">
								<a href="#" class="se-sol-wrap sol-push-right" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
									<div class="se-sol-title">GAME DEVELOPMENT</div>
									<div class="se-sol-img">
										<img src="<?=$global['absolute-url'].'img/sol-game.png';?>" alt="icon">
									</div>
								</a>
							</td>
						</tr>
					</table>
				</div>
				<div class="solution-mobile visible-xs">
					<div class="solution-mbox" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage">
									<img src="<?=$global['absolute-url'].'img/sol-web.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">WEBSITE DEVELOPMENT</div>
							</a>
						</div>
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage">
									<img src="<?=$global['absolute-url'].'img/sol-digital.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">DIGITAL MARKETING</div>
							</a>
						</div>
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage" style="left: 23px;">
									<img src="<?=$global['absolute-url'].'img/sol-mobile.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">MOBILE APPS DEVELOPMENT</div>
							</a>
						</div>
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage">
									<img src="<?=$global['absolute-url'].'img/sol-virtual.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">VIRTUAL / AUGMENTED REALITY</div>
							</a>
						</div>
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage">
									<img src="<?=$global['absolute-url'].'img/sol-video.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">VIDEO ANIMATION</div>
							</a>
						</div>
						<div class="solution-mwrap">
							<a href="#" class="solution-mlink">
								<div class="solution-mimage" style="left: 24px;">
									<img src="<?=$global['absolute-url'].'img/sol-game.png';?>" alt="icon">
								</div>
								<div class="solution-mtitle">GAME DEVELOPMENT</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>