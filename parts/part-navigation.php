<div class="section-navigation hidden-xs">
	<div class="desktop-navigation">
		<div class="container-navigation">
			<ul class="desktop-list">
				<li>
					<a href="<?=$path['home'];?>" class="desktop-link desktop-active">HOME</a>
				</li>
				<li>
					<a href="<?=$path['about'];?>" class="desktop-link">ABOUT US</a>
				</li>
				<li>
					<a href="<?=$path['work'];?>" class="desktop-link">WORK</a>
				</li>
				<li>
					<a href="<?=$path['solution'];?>" class="desktop-link">OUR SOLUTION</a>
				</li>
				<li>
					<a href="<?=$path['news'];?>" class="desktop-link">E-NEWS</a>
				</li>
				<li>
					<a href="<?=$path['career'];?>" class="desktop-link">CAREER</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="desktop-button">
		<div class="container-navigation">
			<div class="desktop-navbutton">
				<a href="javascript:void(0);" class="logo-button">
					<img src="<?=$global['absolute-url'].'img/icon-black.png';?>" alt="icon" class="icon-black">
					<img src="<?=$global['absolute-url'].'img/icon-white.png';?>" alt="icon" class="icon-white" style="display: none;">
				</a>
				<a href="javascript:void(0);" class="bar-button">
					<img src="<?=$global['absolute-url'].'img/bar-black.png';?>" alt="icon" class="bar-nav bar-black">
					<img src="<?=$global['absolute-url'].'img/bar-white.png';?>" alt="icon" class="bar-nav bar-white" style="display: none;">
					<img src="<?=$global['absolute-url'].'img/close-black.png';?>" alt="icon" class="close-nav" style="display: none;">
				</a>
			</div>
		</div>
	</div>
</div>
<div class="mobile-navigation visible-xs">
	<div class="mobile-fixed">
		<div class="mobile-tools">
			<div class="row-tools">
				<div class="col-tools">
					<a href="<?=$path['home'];?>" class="logo-mobile">
						<img src="<?=$global['absolute-url'].'img/logo/logo-eannovate-black.png';?>" alt="logo eannovate">
					</a>
				</div>
				<div class="col-tools">
					<a href="javascript:void(0);" class="btn-menu">
						<img src="<?=$global['absolute-url'].'img/bar-black.png';?>" alt="icon" class="mbtn-bar">
						<img src="<?=$global['absolute-url'].'img/close-black.png';?>" alt="icon" class="mbtn-close" style="display: none;">
					</a>
				</div>
			</div>
		</div>
		<div class="mobile-menu">
			<ul>
				<li>
					<a href="<?=$path['home'];?>" class="mob-link mob-active">HOME</a>
				</li>
				<li>
					<a href="<?=$path['about'];?>" class="mob-link">ABOUT US</a>
				</li>
				<li>
					<a href="<?=$path['work'];?>" class="mob-link">WORK</a>
				</li>
				<li>
					<a href="<?=$path['solution'];?>" class="mob-link">OUR SOLUTION</a>
				</li>
				<li>
					<a href="<?=$path['news'];?>" class="mob-link">E-NEWS</a>
				</li>
				<li>
					<a href="<?=$path['career'];?>" class="mob-link">CAREER</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="mobile-gap"></div>
</div>
<script type="text/javascript">
	function checkScroll() {
		var height_header = $(".top-height").height();
		var scrollTop = $(window).scrollTop();
		if(scrollTop > height_header){
			if($(".desktop-navigation").hasClass("in")){
		   		//nothing
		    } else {
				$('.icon-white').fadeOut();
		    	$('.icon-black').fadeIn();
				$('.bar-white').fadeOut();
		    	$('.bar-black').fadeIn();
		   	}
		} else {
			if($(".desktop-navigation").hasClass("in")){
	    		//nothing
	    	} else {
	        	$('.icon-white').fadeIn();
	        	$('.icon-black').fadeOut();  
				$('.bar-white').fadeIn();
	    		$('.bar-black').fadeOut();
	    	}
		}
	}
	$(document).ready(function() {
		checkScroll();
		$(window).on('scroll', function () {
			checkScroll();
		});
	});
	$(".bar-button").click(function(){
		var height_header = $(".top-height").height();
		var scrollTop = $(window).scrollTop();
		if($(".desktop-navigation").hasClass("in")){
			if(scrollTop > height_header){
				$(".bar-black").fadeIn();
			} else {
				$(".bar-white").fadeIn();
			}
			if(scrollTop > height_header){
				$(".icon-black").fadeIn();
			} else {
				$(".icon-white").fadeIn();
			}
			$(".close-nav").fadeOut();
			$(".desktop-navigation").removeClass("in");
			$(".desktop-navigation").fadeOut();
		} else {
			$(".icon-black").fadeIn();
			$(".icon-white").fadeOut();
			$(".bar-nav").fadeOut();
			$(".close-nav").fadeIn();
			$(".desktop-navigation").addClass("in");
			$(".desktop-navigation").fadeIn();
		}
	});
	$(".btn-menu").click(function(){
		$(".mobile-menu").slideToggle();
		if($(".mobile-menu").hasClass("in")){
			$(".mbtn-close").fadeOut();
			$(".mbtn-bar").fadeIn();
			$(".mobile-menu").removeClass("in");
		} else {
			$(".mbtn-bar").fadeOut();
			$(".mbtn-close").fadeIn();
			$(".mobile-menu").addClass("in");
		}
	});
</script>