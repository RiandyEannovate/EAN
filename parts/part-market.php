<div class="section-market">
	<div class="container container-ean" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
		<div class="sm-note">
			Be in control. Connect with us.
		</div>
		<div class="sm-text">
			WHOEVER REACH THE MARKET FIRST HAVE GREATER CHANCE TO CONTROL THEM
		</div>
		<div class="sm-link">
			<a href="#modalContact" data-toggle="modal" data-target="#modalContact">GET IN TOUCH</a>
		</div>
	</div>
</div>

<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog cmodal-dialog" role="document">
	    	<div class="modal-content cmodal-dialog">
	      		<div class="modal-body cmodal-body">
	        		<div class="cmodal-head">CONTACT US</div>
	        		<div class="cmodal-note">Our team are more than happy to help you</div>
	        		<div class="cmodal-form">
		        		<form id="form-contact" name="contact" action="index.php?action=contact" enctype="multipart/form-data" method="post" onsubmit="return validContact()">
		        			<div id="cinput-error" class="error-text"></div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-name" name="name" class="form-control cmodal-input" placeholder="your name">
		        			</div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-email" name="email" class="form-control cmodal-input" placeholder="your e-mail address">
		        			</div>
		        			<div class="cmodal-content">
		        				<input type="text" id="cinput-subject" name="subject" class="form-control cmodal-input" placeholder="what it is about">
		        			</div>
		        			<div class="cmodal-content">
		        				<textarea rows="6" id="cinput-message" name="message" class="form-control cmodal-input" placeholder="your message"></textarea>
		        			</div>
		        			<div class="cmodal-submit">
		        				<button type="submit" class="btn btn-submit">SEND</button>
		        			</div>
		        		</form>
	        		</div>
	      		</div>
	    	</div>
	  	</div>
	</div>