<div class="section-header top-height">
	<div class="header-logo">
		<a href="<?=$path['home'];?>">
			<img src="<?=$global['logo-white'];?>" alt="PT.Eannovate Creative Technology">
		</a>
	</div>
	<div class="header-link">
		<a href="javascript:void(0);" class="scroll" target="section-eannovate">CROSS THE BRIDGE TO YOUR SUCCESS TODAY</a>
	</div>
</div>