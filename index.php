<?php 
include("packages/require.php");
$curpage='home';
$cur_url='';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-home'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
	<meta name="description" content="<?php echo $seo['desc-home'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
	<script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<?php include("parts/part-header.php");?>
	<!-- END SECTION HEADER -->

	<!-- START SECTION EANNOVATE -->
	<?php include("parts/part-about.php");?>
	<!-- START SECTION EANNOVATE -->

	<!-- START SECTION CREATE IDEA -->
	<?php include("parts/part-idea.php");?>
	<!-- END SECCTION CREATE IDEA -->

	<!-- START SECTION CLIENT -->
	<?php include("parts/part-client.php");?>
	<!-- END SECTION CLIENT -->

	<!-- START SECTION PROJECT -->
	<?php include("parts/part-project.php");?>
	<!-- END SECTION PROJECT -->

	<!-- START SECTION MIDDLE ICON -->
	<?php include("parts/part-icon.php");?>
	<!-- END SECTION MIDDLE ICON -->

	<!-- START SECTION MARKET TEXT -->
	<?php include("parts/part-market.php");?>
	<!-- END SECCTION MARKET TEXT -->
	
	<!-- START SECTION CREATOR TEXT -->
	<?php include("parts/part-creator.php");?>
	<!-- END SECCTION CREATOR TEXT -->
	
	<!-- START SECTION NEWS -->
	<?php include("parts/part-news.php");?>
	<!-- END SECTION NEWS -->
	
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->

	<script src="<?=$global['absolute-url'];?>js/home.js?<?=mt_rand(10,1000);?>"></script>
</body>
</html>