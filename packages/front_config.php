<?php 
$global['absolute-url'] = "https://www.eannovate.com/dev/ean/";
$global['root-url'] = $_SERVER['DOCUMENT_ROOT']."/dev/ean/";
$global['root-url-model'] = $global['root-url']."model/";
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['absolute-url-admin'] = $global['absolute-url']."admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-white'] = $global['absolute-url']."img/logo/logo-eannovate-white.png";
$global['logo-black'] = $global['absolute-url']."img/logo/logo-eannovate-black.png";
$seo['company-name'] = "PT Eannovate Creative Technology";
$global['copyright'] = "&copy; Copyright ".date('Y')." Eannovate. All rights reserved.";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";

$path['home'] = $global['absolute-url'];
$path['about'] = $global['absolute-url']."about.html";
$path['solution'] = $global['absolute-url']."solution.html";
$path['career'] = $global['absolute-url']."career.html";
$path['work'] = $global['absolute-url']."work.html";
$path['work-detail'] = $global['absolute-url']."work/";
$path['news'] = $global['absolute-url']."news.html";
$path['news-detail'] = $global['absolute-url']."news/";
$path['solution'] = $global['absolute-url']."solution.html";
$path['solution-detail'] = $global['absolute-url']."solution/";

$seo['slogan'] = "Web, Mobile Android,iOS App Developer in Jakarta, Indonesia";

//seo home
$seo['title-home'] = $seo['company-name']." | ".$seo['slogan'];
$seo['keyword-home'] = "";
$seo['desc-home'] = "";

//seo about
$seo['title-about'] = $seo['company-name']." | About Us";
$seo['keyword-about'] = "";
$seo['desc-about'] = "";

//seo work
$seo['title-work'] = $seo['company-name']." | Our Work";
$seo['keyword-work'] = "";
$seo['desc-work'] = "";

//seo solution
$seo['title-solution'] = $seo['company-name']." | Our Solution";
$seo['keyword-solution'] = "";
$seo['desc-solution'] = "";

//seo news
$seo['title-news'] = $seo['company-name']." | News";
$seo['keyword-news'] = "";
$seo['desc-news'] = "";
?>