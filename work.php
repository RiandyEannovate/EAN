<?php 
include("packages/require.php");
$curpage='work';
$cur_url='work.html';
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $seo['title-work'];?></title>
	<meta name="keywords" content="<?php echo $seo['keyword-work'];?>">
	<meta name="description" content="<?php echo $seo['desc-work'];?>">
	<?php include("packages/head-new.php");?>
	<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/work.css?<?=mt_rand(10,1000);?>" media="screen"/>
</head>
<body>
	<!-- START SECTION NAVIGATION -->
	<?php include("parts/part-navigation.php");?>
	<!-- END SECTION NAVIGATION -->

	<!-- START SECTION HEADER -->
	<div class="header-work top-height">
		<div class="container container-ean">
			<div class="header-wposition">
				<div class="header-wcontent">
					<div class="header-wnote">OUR CASE STUDIES</div>
					<div class="header-wtext">
						WE FOCUS ON DELIVERING INNOVATION TO YOUR BUSINESS
					</div>
					<div class="header-wborder"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END SECTION HEADER -->

	<div class="section-work">
		<div class="container container-ean">
			<div class="work-filter">
				<ul>
					<li>
						<a href="#" class="wfilter-link wfilter-active">ALL</a>
					</li>
					<li>
						<a href="#" class="wfilter-link">CLIENT WORK</a>
					</li>
					<li>
						<a href="#" class="wfilter-link">OUR WORK</a>
					</li>
				</ul>
			</div>

			<div class="work-list">
				<div class="row-work">
					<?php for($w=1;$w<=10;$w++){ ?>
					<div class="col-work">
						<div class="work-wrap">
							<a href="<?=$path['work-detail'];?>" class="work-image" style="background-image: url('<?=$global['absolute-url'].'img/news5.png';?>')"></a>
							<div class="work-content">
								<div class="work-category">CLIENT WORK</div>
								<a href="<?=$path['work-detail'];?>" class="work-title">KANEKIN.CO</a>
								<div class="work-desc">
									Kanekin initiated as a creative agency handling
									brand experience design and marketing of various
									brands in indonesia
								</div>
								<div class="work-link">
									<a href="<?=$path['work-detail'];?>">
										<span>SEE MORE</span>
										<hr/>
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>	
				<div class="work-more">
					<a href="javascript:void(0);" class="btn-more">SEE MORE</a>
				</div>
			</div>

		</div>
	</div>
	
	
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->
</body>
</html>